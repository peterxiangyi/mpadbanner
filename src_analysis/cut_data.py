#!/user/bin/python
# encoding=utf-8
import random

from config import train_rate

# train_rate = 0.8

R = []
with open('../data/RB_reindex.txt', 'r') as infile:
    for line in infile.readlines():
        user, item, rating = line.strip().split('\t')
        R.append([user, item, rating])
        # R.append([user, item, Clicks])

random.shuffle(R)

train_num = int(len(R) * train_rate)

with open('../data/RB_reindex' + '.train', 'w') as trainfile,\
        open('../data/RB_reindex' + '.test', 'w') as testfile:
    for r in R[:train_num]:
        trainfile.write('\t'.join(r) + '\n')
    for r in R[train_num:]:
        testfile.write('\t'.join(r) + '\n')
