#!/usr/bin/python
# encoding=utf-8

""" All parameters are configged here """

###########################################################################
# the url of Neo4j, user name, password
url = "http://10.6.55.242:7474"
user_name = "neo4j"
pass_word = "1q2w3e4r"
db_name = "neo4j"

###########################################################################
# initial setting of parameters used in the model
# train-test ratio
train_rate = 0.8

# embedding parameters
dim = 128
walk_len = 5
win_size = 3
num_walk = 10

# meta path selected
metapaths = ['RBR', 'BRB', 'BPrB', 'BShB', 'BSiB', 'RBPrBR', 'RBShBR', 'RBSiBR', 'BLaTLaB',
             'BLaLcLaB', 'BBaCBaB', 'BPCPB', 'BPLcPB', 'BLoLcLoB', 'RBLaTLaBR', 'RBLaLcLaBR', 'RBBaCBaBR', 'RBPCPBR', 'RBPLcPBR', 'RBLoLcLoBR']

user_metapaths = ['RBR', 'RBPrBR', 'RBShBR', 'RBSiBR', 'RBLaTLaBR',
                  'RBLaLcLaBR', 'RBBaCBaBR', 'RBPCPBR', 'RBPLcPBR', 'RBLoLcLoBR']

item_metapaths = ['BRB', 'BPrB', 'BShB', 'BSiB', 'BLaTLaB',
                  'BLaLcLaB', 'BBaCBaB', 'BPCPB', 'BPLcPB', 'BLoLcLoB']

# metapath fusion parameters
ratedim = 10
userdim = 30
itemdim = 10

steps = 100
delta = 0.02
beta_e = 0.1
beta_h = 0.1
beta_p = 2
beta_w = 0.1
beta_b = 0.1
reg_u = 1.0
reg_v = 1.0
error_level = 0.000001
