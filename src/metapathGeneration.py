
# import sys
import numpy as np
import pandas as pd
# import random

from utils import load_binary_relationship, generage_onestep_metapath, generage_twostep_metapath, generage_threestep_metapath


# get number of entities
num = pd.read_csv("../data/num.csv", header=None)
rnum, bnum, prnum, shnum, sinum, banum, pnum, lonum, lanum, inum, cnum, lcnum, tnum, tenum \
    = num[0].values.tolist()
numlist = num[0].values.tolist()


##############################################################################################
# 1. Load basic binary relationship matrix
##############################################################################################

RB = load_binary_relationship('../data/RB_reindex.train', rnum, bnum)
BPr = load_binary_relationship('../data/BPr_reindex.txt', bnum, prnum)
BSh = load_binary_relationship('../data/BSh_reindex.txt', bnum, shnum)
BSi = load_binary_relationship('../data/BSi_reindex.txt', bnum, sinum)
BBa = load_binary_relationship('../data/BBa_reindex.txt', bnum, banum)
BP = load_binary_relationship('../data/BP_reindex.txt', bnum, pnum)
BLo = load_binary_relationship('../data/BLo_reindex.txt', bnum, lonum)
BLa = load_binary_relationship('../data/BLa_reindex.txt', bnum, lanum)
BaC = load_binary_relationship('../data/BaC_reindex.txt', banum, cnum)
PC = load_binary_relationship('../data/PC_reindex.txt', pnum, cnum)
PLc = load_binary_relationship('../data/PLc_reindex.txt', pnum, lcnum)
LoLc = load_binary_relationship('../data/LoLc_reindex.txt', lonum, lcnum)
LaLc = load_binary_relationship('../data/LaLc_reindex.txt', lanum, lcnum)
LaT = load_binary_relationship('../data/LaT_reindex.txt', lanum, tnum)


##############################################################################################
# 2. generate one step meta-path
##############################################################################################

# RBR
generage_onestep_metapath(RB, '../data/metapath/RBR.txt')
# BRB
generage_onestep_metapath((RB.T), '../data/metapath/BRB.txt')
# BPrB
generage_onestep_metapath(BPr, '../data/metapath/BPrB.txt')
# BShB
generage_onestep_metapath(BSh, '../data/metapath/BShB.txt')
# BSiB
generage_onestep_metapath(BSi, '../data/metapath/BSiB.txt')


##############################################################################################
# 3. generate two steps meta-path
##############################################################################################

# RBPrBR
generage_twostep_metapath(RB, BPr, '../data/metapath/RBPrBR.txt')
# RBShBR
generage_twostep_metapath(RB, BSh, '../data/metapath/RBShBR.txt')
# RBSiBR
generage_twostep_metapath(RB, BSi, '../data/metapath/RBSiBR.txt')

# BLaTLaB
generage_twostep_metapath(BLa, LaT, '../data/metapath/BLaTLaB.txt')
# BLaLcLaB
generage_twostep_metapath(BLa, LaLc, '../data/metapath/BLaLcLaB.txt')
# BBaCBaB
generage_twostep_metapath(BBa, BaC, '../data/metapath/BBaCBaB.txt')
# BPCPB
generage_twostep_metapath(BP, PC, '../data/metapath/BPCPB.txt')
# BPLcPB
generage_twostep_metapath(BP, PLc, '../data/metapath/BPLcPB.txt')
# BLoLcLoB
generage_twostep_metapath(BLo, LoLc, '../data/metapath/BLoLcLoB.txt')


##############################################################################################
# 4. generate three steps meta-path
##############################################################################################

# RBLaTLaBR
generage_threestep_metapath(RB, BLa, LaT, '../data/metapath/RBLaTLaBR.txt')
# RBLaLcLaBR
generage_threestep_metapath(RB, BLa, LaLc, '../data/metapath/RBLaLcLaBR.txt')
# RBBaCBaBR
generage_threestep_metapath(RB, BBa, BaC, '../data/metapath/RBBaCBaBR.txt')
# RBPCPBR
generage_threestep_metapath(RB, BP, PC, '../data/metapath/RBPCPBR.txt')
# RBPLcPBR
generage_threestep_metapath(RB, BP, PLc, '../data/metapath/RBPLcPBR.txt')
# RBLoLcLoBR
generage_threestep_metapath(RB, BLo, LoLc, '../data/metapath/RBLoLcLoBR.txt')
