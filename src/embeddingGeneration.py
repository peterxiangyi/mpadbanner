import os

from config import dim, walk_len, win_size, num_walk
from config import metapaths


for metapath in metapaths:
    input_file = '../data/metapath/' + metapath + '.txt'
    output_file = '../data/embeddings/' + metapath + '.embedding'

    cmd = 'deepwalk --format edgelist --input ' + input_file + ' --output ' + output_file + \
          ' --walk-length ' + str(walk_len) + ' --window-size ' + str(win_size) + ' --number-walks '\
        + str(num_walk) + ' --representation-size ' + str(dim)

    print(cmd)
    os.system(cmd)
