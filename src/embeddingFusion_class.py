#!/user/bin/python
# encoding=utf-8
import numpy as np
import pandas as pd
from datetime import datetime
from math import sqrt
import sys
from config import steps, delta, beta_b, beta_e, beta_h, beta_p, beta_w, reg_u, reg_v, delta, error_level
from config import ratedim, userdim, itemdim, train_rate
from config import user_metapaths, item_metapaths


class HNERec:
    def __init__(self, unum, inum, ratedim, userdim, itemdim, user_metapaths, item_metapaths, trainfile, testfile, steps, delta, beta_e, beta_h, beta_p, beta_w, beta_b, reg_u, reg_v, error_level):
        self.unum = unum
        self.inum = inum
        self.ratedim = ratedim
        self.userdim = userdim
        self.itemdim = itemdim
        self.steps = steps
        self.delta = delta
        self.beta_e = beta_e
        self.beta_h = beta_h
        self.beta_p = beta_p
        self.beta_w = beta_w
        self.beta_b = beta_b
        self.reg_u = reg_u
        self.reg_v = reg_v
        self.error_level = error_level

        self.user_metapathnum = len(user_metapaths)
        self.item_metapathnum = len(item_metapaths)

        self.X, self.user_metapathdims = self.load_embedding(
            user_metapaths, unum)
        print('Load user embeddings finished.')

        self.Y, self.item_metapathdims = self.load_embedding(
            item_metapaths, inum)
        print('Load item embeddings finished.')

        self.R, self.T, self.ba = self.load_rating(trainfile, testfile)
        print('Load rating finished.')
        print('train size : ', len(self.R))
        print('test size : ', len(self.T))

        self.initialize()
        self.recommend()

    def load_embedding(self, metapaths, num):
        X = {}
        for i in range(num):
            X[i] = {}
        metapathdims = []

        ctn = 0
        for metapath in metapaths:
            sourcefile = '../data/embeddings/' + metapath

            with open(sourcefile) as infile:

                k = int(infile.readline().strip().split(' ')[1])
                metapathdims.append(k)
                for i in range(num):
                    X[i][ctn] = np.zeros(k)

                n = 0
                for line in infile.readlines():
                    n += 1
                    arr = line.strip().split(' ')
                    i = int(arr[0])
                    for j in range(k):
                        X[i][ctn][j] = float(arr[j + 1])
                print('metapath ', metapath, 'numbers ', n)
            ctn += 1
        return X, metapathdims

    def load_rating(self, trainfile, testfile):
        R_train = []
        R_test = []
        ba = 0.0
        n = 0

        with open(trainfile) as infile:
            for line in infile.readlines():
                user, item, rating = line.strip().split('\t')
                R_train.append([int(user), int(item), float(rating)])
                ba += float(rating)
                n += 1
        ba = ba / n
        # ba = 0
        with open(testfile) as infile:
            for line in infile.readlines():
                user, item, rating = line.strip().split('\t')

                R_test.append([int(user), int(item), float(rating)])

        return R_train, R_test, ba

    def initialize(self):
        self.E = np.random.randn(self.unum, self.itemdim) * 0.1
        self.H = np.random.randn(self.inum, self.userdim) * 0.1
        self.U = np.random.randn(self.unum, self.ratedim) * 0.1
        self.V = np.random.randn(self.inum, self.ratedim) * 0.1

        self.pu = np.ones((self.unum, self.user_metapathnum)
                          ) * 1.0 / self.user_metapathnum
        self.pv = np.ones((self.inum, self.item_metapathnum)
                          ) * 1.0 / self.item_metapathnum

        self.Wu = {}
        self.bu = {}
        for k in range(self.user_metapathnum):
            self.Wu[k] = np.random.randn(
                self.userdim, self.user_metapathdims[k]) * 0.1
            self.bu[k] = np.random.randn(self.userdim) * 0.1

        self.Wv = {}
        self.bv = {}
        for k in range(self.item_metapathnum):
            self.Wv[k] = np.random.randn(
                self.itemdim, self.item_metapathdims[k]) * 0.1
            self.bv[k] = np.random.randn(self.itemdim) * 0.1

    def sigmod(self, x):
        return 1 / (1 + np.exp(-x))

    def cal_u(self, i):
        ui = np.zeros(self.userdim)
        for k in range(self.user_metapathnum):
            ui += self.pu[i][k] * \
                self.sigmod((self.Wu[k].dot(self.X[i][k]) + self.bu[k]))
        return self.sigmod(ui)

    def cal_v(self, j):
        vj = np.zeros(self.itemdim)
        for k in range(self.item_metapathnum):
            vj += self.pv[j][k] * \
                self.sigmod((self.Wv[k].dot(self.Y[j][k]) + self.bv[k]))
        return self.sigmod(vj)

    def get_rating(self, i, j):
        ui = self.cal_u(i)
        vj = self.cal_v(j)
        return self.U[i, :].dot(self.V[j, :]) + self.reg_u * ui.dot(self.H[j, :]) + self.reg_v * self.E[i, :].dot(vj)

    def maermse(self):
        m = 0.0
        mae = 0.0
        rmse = 0.0
        n = 0
        for t in self.T:
            n += 1
            i = t[0]
            j = t[1]
            r = t[2]
            r_p = self.get_rating(i, j)

            m = abs(r_p - r)
            mae += m
            rmse += m * m
        mae = mae * 1.0 / n
        rmse = sqrt(rmse * 1.0 / n)
        return mae, rmse

    def recommend(self):
        print('\nStart training')
        mae = []
        rmse = []
        starttime = datetime.now()
        perror = 99999
        cerror = 9999
        n = len(self.R)

        for step in range(steps):
            total_error = 0.0
            for t in self.R:
                i = t[0]
                j = t[1]
                rij = t[2]

                rij_t = self.get_rating(i, j)
                eij = rij - rij_t
                total_error += eij * eij

                U_g = -eij * self.V[j, :] + self.beta_e * self.U[i, :]
                V_g = -eij * self.U[i, :] + self.beta_h * self.V[j, :]

                self.U[i, :] -= self.delta * U_g
                self.V[j, :] -= self.delta * V_g

                ui = self.cal_u(i)
                for k in range(self.user_metapathnum):
                    x_t = self.sigmod(self.Wu[k].dot(
                        self.X[i][k]) + self.bu[k])

                    pu_g = self.reg_u * -eij * \
                        (ui * (1-ui) * self.H[j, :]).dot(x_t) + \
                        self.beta_p * self.pu[i][k]

                    Wu_g = self.reg_u * -eij * self.pu[i][k] * np.array([ui * (1-ui) * x_t * (
                        1-x_t) * self.H[j, :]]).T.dot(np.array([self.X[i][k]])) + self.beta_w * self.Wu[k]
                    bu_g = self.reg_u * -eij * ui * \
                        (1-ui) * self.pu[i][k] * self.H[j, :] * \
                        x_t * (1-x_t) + self.beta_b * self.bu[k]

                    self.pu[i][k] -= 0.1 * self.delta * pu_g
                    self.Wu[k] -= 0.1 * self.delta * Wu_g
                    self.bu[k] -= 0.1 * self.delta * bu_g

                H_g = self.reg_u * -eij * ui + self.beta_e * self.H[j, :]
                self.H[j, :] -= self.delta * H_g

                vj = self.cal_v(j)
                for k in range(self.item_metapathnum):
                    y_t = self.sigmod(self.Wv[k].dot(
                        self.Y[j][k]) + self.bv[k])
                    pv_g = self.reg_v * -eij * \
                        (vj * (1-vj) * self.E[i, :]).dot(y_t) + \
                        self.beta_p * self.pv[j][k]
                    Wv_g = self.reg_v * -eij * self.pv[j][k] * np.array([vj * (1-vj) * y_t * (
                        1 - y_t) * self.E[i, :]]).T.dot(np.array([self.Y[j][k]])) + self.beta_w * self.Wv[k]
                    bv_g = self.reg_v * -eij * vj * \
                        (1-vj) * self.pv[j][k] * self.E[i, :] * \
                        y_t * (1 - y_t) + self.beta_b * self.bv[k]

                    self.pv[j][k] -= 0.1 * self.delta * pv_g
                    self.Wv[k] -= 0.1 * self.delta * Wv_g
                    self.bv[k] -= 0.1 * self.delta * bv_g

                E_g = self.reg_v * -eij * vj + beta_h * self.E[i, :]
                self.E[i, :] -= self.delta * E_g

            perror = cerror
            cerror = total_error / n

            self.delta = 0.93 * self.delta

            if(abs(perror - cerror) < self.error_level):
                break

            print('Step {} finished'.format(step))
            print('cerror: ', sqrt(cerror))

            # Calculate MAE and RSME in test data
            MAE, RMSE = self.maermse()
            mae.append(MAE)
            rmse.append(RMSE)
            endtime = datetime.now()
            print('time: ', endtime - starttime)

        print('MAE: ', min(mae), ' RMSE: ', min(rmse))

    def get_reverse_index(self, file_path):
        """reverse index of node ids"""
        num_file = pd.read_csv(file_path, header=None)
        num_file.columns = ['node_id', 'reindex']
        return num_file.set_index('reindex').to_dict()

    def reverse_index(self, src_file, dst_file):
        """output result on original ids"""
        srcFile = self.get_reverse_index(src_file)
        dstFile = self.get_reverse_index(dst_file)

        results = []
        for t in self.T:
            i = t[0]
            j = t[1]
            rij = t[2]

            rij_t = self.get_rating(i, j)
            i_nodeid = srcFile['node_id'][i]
            j_nodeid = dstFile['node_id'][j]
            results.append([i, j, i_nodeid, j_nodeid, rij, rij_t,
                            abs(rij-rij_t), abs((rij-rij_t)/rij)])

        return results

    def get_output(self, src_file, dst_file):
        # dimensions of matrix
        print('U shape: {}, V shape: {}, pu shape: {}, H shape: {}, pv shape: {}, E shape: {}'.format(self.U.shape, self.V.shape,
                                                                                                      self.pu.shape, self.H.shape, self.pv.shape, self.E.shape))

        print('Wu shape: ({}, {}, {})'.format(
            len(self.Wu), len(self.Wu[0]), len(self.Wu[0][0])))
        print('bu shape: ({}, {})'.format(len(self.bu), len(self.bu[0])))
        print('Wv shape: ({}, {}, {})'.format(
            len(self.Wv), len(self.Wv[0]), len(self.Wv[0][0])))
        print('bv shape: ({}, {})'.format(len(self.bv), len(self.bv[0])))

        print('X shape: ({}, {}, {})'.format(
            len(self.X), len(self.X[0]), len(self.X[0][0])))
        print('Y shape: ({}, {}, {})'.format(
            len(self.Y), len(self.Y[0]), len(self.Y[0][0])))

        print('User metapathnum: {}, user metapathdims: {}'.format(
            self.user_metapathnum, self.user_metapathdims))
        print('Item metapathnum: {}, item metapathdims: {}'.format(
            self.item_metapathnum, self.item_metapathdims))

        # results evaluation of test data
        results = self.reverse_index(src_file, dst_file)
        results = pd.DataFrame(results)
        results.columns = ['p_id_reindex', 'b_id_reindex',
                           'p_id', 'b_id', 'rat', 'pred', 'diff', 'diff_pct']
        results.to_csv('../results/test_data_prediction.csv',
                       encoding='utf8', index=None)

        print('\nThe results are as follows:\n')
        print(results.head())


if __name__ == "__main__":

    num = pd.read_csv("../data/num.csv", header=None)
    unum, inum, *arg = num[0].values.tolist()

    for i in range(len(user_metapaths)):
        user_metapaths[i] += '.embedding'
    for i in range(len(item_metapaths)):
        item_metapaths[i] += '.embedding'

    trainfile = '../data/RB_reindex' + '.train'
    testfile = '../data/RB_reindex' + '.test'

    print('train_rate: ', train_rate)
    print('ratedim: ', ratedim, ' userdim: ', userdim, ' itemdim: ', itemdim)
    print('max_steps: ', steps)
    print('delta: ', delta, 'beta_e: ', beta_e, 'beta_h: ', beta_h, 'beta_p: ',
          beta_p, 'beta_w: ', beta_w, 'beta_b', beta_b, 'reg_u', reg_u, 'reg_v', reg_v)

    model = HNERec(unum, inum, ratedim, userdim, itemdim, user_metapaths, item_metapaths, trainfile,
                   testfile, steps, delta, beta_e, beta_h, beta_p, beta_w, beta_b, reg_u, reg_v, error_level)

    model.get_output('../data/num_Placement.csv', '../data/num_Banner.csv')
