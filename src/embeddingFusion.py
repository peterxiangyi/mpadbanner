#!/user/bin/python
# encoding=utf-8
import numpy as np
import pandas as pd
from datetime import datetime


from utils import load_embedding, load_rating, initialize, recommend, get_output

from config import user_metapaths, item_metapaths
from config import ratedim, userdim, itemdim, train_rate
from config import steps, delta, beta_b, beta_e, beta_h, beta_p, beta_w, reg_u, reg_v, delta, error_level


# 1. get parameters value
num = pd.read_csv("../data/num.csv", header=None)
unum, inum, *arg = num[0].values.tolist()


# 2. get embeddings and data

# get embeddings and data
user_metapathnum = len(user_metapaths)
item_metapathnum = len(item_metapaths)

for i in range(len(user_metapaths)):
    user_metapaths[i] += '.embedding'

for i in range(len(item_metapaths)):
    item_metapaths[i] += '.embedding'

trainfile = '../data/RB_reindex' + '.train'
testfile = '../data/RB_reindex' + '.test'

print('train_rate: ', train_rate)
print('ratedim: ', ratedim, ' userdim: ', userdim, ' itemdim: ', itemdim)
print('max_steps: ', steps)
print('delta: {}, reg_u: {}, reg_v: {}'.format(delta, reg_u, reg_v))
print('beta_e: {}, beta_h: {}, beta_p: {}, beta_w: {}, beta_b: {}'.format(
    beta_e, beta_h, beta_p, beta_w, beta_b))


# 3. Start embedding fusion

# Load replacement and banner embeddings
X, user_metapathdims = load_embedding(user_metapaths, unum)
print('Load placement embeddings finished.')
print('X shape: ({}, {}, {})'.format(len(X), len(X[0]), len(X[0][0])))

Y, item_metapathdims = load_embedding(item_metapaths, inum)
print('Load banner embeddings finished.')
print('Y shape: ({}, {}, {})'.format(len(Y), len(Y[0]), len(Y[0][0])))

# load ratings
R, T, ba = load_rating(trainfile, testfile)
print('Load rating finished.')
print('train size : ', len(R))
print('test size : ', len(T))

# Initialization
E, H, U, V, pu, pv, Wu, bu, Wv, bv = initialize(
    unum, inum, userdim, itemdim, ratedim, user_metapathnum, item_metapathnum, user_metapathdims, item_metapathdims)

# Recommendation
U, V, pu, Wu, bu, H, pv, Wv, bv, E = recommend(steps, R, T, V, U, H, E, X, Y, user_metapathnum, item_metapathnum, userdim, itemdim,
                                               pu, Wu, bu, pv, Wv, bv, reg_u, reg_v, beta_e, beta_h, beta_p, beta_w, beta_b, delta, error_level)


# 4. get results

get_output('../data/num_Placement.csv', '../data/num_Banner.csv', T, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
           pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y, user_metapathdims, item_metapathdims)
