import numpy as np
import pandas as pd
from math import sqrt
from datetime import datetime


def get_reindex(file_path):
    """reindex of node ids"""
    num_file = pd.read_csv(file_path, header=None)
    num_file.columns = ['node_id', 'reindex']
    return num_file.set_index('node_id').to_dict()


def reindex(file_path, src_file, dst_file):
    """get reindex of relationship matrix"""
    srcFile = get_reindex(src_file)
    dstFile = get_reindex(dst_file)
    data = pd.read_csv(file_path, sep="\t", header=None)

    src_index = []
    dst_index = []
    for i in range(data.shape[0]):
        index1 = int(data[0][i])
        src_index.append(srcFile['reindex'][index1])
        index2 = int(data[1][i])
        dst_index.append(dstFile['reindex'][index2])

    data['src_index'] = src_index
    data['dst_index'] = dst_index
    data['rating'] = data[2]  # Other RM has only 3 colums, [2]-rating
    data = data[['src_index', 'dst_index', 'rating']]

    return data


def reindex_rb(file_path, src_file, dst_file):
    """get reindex of relationship matrix
    For RB matrix, there is one additional column: Clicks. 
    If you want to use Clicks instead of CTR, pls use this funciton for RB relation matrix.
    """
    srcFile = get_reindex(src_file)
    dstFile = get_reindex(dst_file)
    data = pd.read_csv(file_path, sep="\t", header=None)

    src_index = []
    dst_index = []
    for i in range(data.shape[0]):
        index1 = int(data[0][i])
        src_index.append(srcFile['reindex'][index1])
        index2 = int(data[1][i])
        dst_index.append(dstFile['reindex'][index2])

    data['src_index'] = src_index
    data['dst_index'] = dst_index
    data['rating'] = data[3]  # RB has 4 columns, [2]-CTR, [3]-Clicks
    data = data[['src_index', 'dst_index', 'rating']]

    return data


def load_binary_relationship(file, src_dim, dst_dim):
    """load binary relationship matrix"""
    RM = np.zeros((src_dim, dst_dim))
    with open(file, 'r') as infile:
        for line in infile.readlines():
            src_id, dst_id, rating = line.strip().split('\t')
            RM[int(src_id)][int(dst_id)] = rating
    return RM


def generage_onestep_metapath(relMat, targetFile):
    """generate one step metapath"""
    print('Start...')

    mm = (relMat.dot(relMat.T))
    print(mm.shape)
    print('writing to file...')
    total = 0
    with open(targetFile, 'w') as outfile:
        for i in range(mm.shape[0]):
            for j in range(mm.shape[1]):
                if mm[i][j] != 0 and i != j:
                    outfile.write(str(i) + '\t' + str(j) +
                                  '\t' + str(mm[i][j]) + '\n')
                    total += 1
    print('total = ', total)


def generage_twostep_metapath(rm1, rm2, targetFile):
    """generate two steps metapath"""
    print('Start...')

    mm = (rm1.dot(rm2).dot(rm2.T).dot(rm1.T))
    print(mm.shape)
    print('writing to file...')
    total = 0
    with open(targetFile, 'w') as outfile:
        for i in range(mm.shape[0]):
            for j in range(mm.shape[1]):
                if mm[i][j] != 0 and i != j:
                    outfile.write(str(i) + '\t' + str(j) +
                                  '\t' + str(mm[i][j]) + '\n')
                    total += 1
    print('total = ', total)


def generage_threestep_metapath(rm1, rm2, rm3, targetFile):
    """generate three steps metapath"""
    print('Start...')

    mm = (rm1.dot(rm2).dot(rm3).dot(rm3.T).dot(rm2.T).dot(rm1.T))
    print(mm.shape)
    print('writing to file...')
    total = 0
    with open(targetFile, 'w') as outfile:
        for i in range(mm.shape[0]):
            for j in range(mm.shape[1]):
                if mm[i][j] != 0 and i != j:
                    outfile.write(str(i) + '\t' + str(j) +
                                  '\t' + str(mm[i][j]) + '\n')
                    total += 1
    print('total = ', total)


def load_embedding(metapaths, num):
    """load embeddings"""
    X = {}
    for i in range(num):
        X[i] = {}
    metapathdims = []

    ctn = 0
    for metapath in metapaths:
        sourcefile = '../data/embeddings/' + metapath
        # print sourcefile
        with open(sourcefile) as infile:

            k = int(infile.readline().strip().split(' ')[1])
            metapathdims.append(k)
            for i in range(num):
                X[i][ctn] = np.zeros(k)

            n = 0
            for line in infile.readlines():
                n += 1
                arr = line.strip().split(' ')
                # i = int(arr[0]) - 1
                i = int(arr[0])
                for j in range(k):
                    X[i][ctn][j] = float(arr[j + 1])
            print('metapath ', metapath, 'numbers ', n)
        ctn += 1
    return X, metapathdims


def load_rating(trainfile, testfile):
    """load ratings"""
    R_train = []
    R_test = []
    ba = 0.0
    n = 0
    # user_test_dict = dict()
    with open(trainfile) as infile:
        for line in infile.readlines():
            user, item, rating = line.strip().split('\t')
            # R_train.append([int(user)-1, int(item)-1, int(rating)])
            # add float()
            R_train.append([int(user), int(item), float(rating)])
            ba += float(rating)
            n += 1
    ba = ba / n
    # ba = 0
    with open(testfile) as infile:
        for line in infile.readlines():
            user, item, rating = line.strip().split('\t')
            # R_test.append([int(user)-1, int(item)-1, int(rating)])
            R_test.append([int(user), int(item), float(rating)])  # add float()
    return R_train, R_test, ba


def initialize(unum, inum, userdim, itemdim, ratedim, user_metapathnum, item_metapathnum, user_metapathdims, item_metapathdims):
    """initialization of fusion parameters"""
    E = np.random.randn(unum, itemdim) * 0.1
    H = np.random.randn(inum, userdim) * 0.1
    U = np.random.randn(unum, ratedim) * 0.1
    V = np.random.randn(inum, ratedim) * 0.1

    pu = np.ones((unum, user_metapathnum)) * 1.0 / user_metapathnum
    pv = np.ones((inum, item_metapathnum)) * 1.0 / item_metapathnum

    Wu = {}
    bu = {}
    for k in range(user_metapathnum):
        Wu[k] = np.random.randn(userdim, user_metapathdims[k]) * 0.1
        bu[k] = np.random.randn(userdim) * 0.1

    Wv = {}
    bv = {}
    for k in range(item_metapathnum):
        Wv[k] = np.random.randn(itemdim, item_metapathdims[k]) * 0.1
        bv[k] = np.random.randn(itemdim) * 0.1

    return E, H, U, V, pu, pv, Wu, bu, Wv, bv


def sigmod(x):
    """sigmod function"""
    return 1 / (1 + np.exp(-x))


def cal_u(i, userdim, user_metapathnum, pu, Wu, bu, X):
    """calculate user embedding"""
    ui = np.zeros(userdim)
    for k in range(user_metapathnum):
        ui += pu[i][k] * sigmod((Wu[k].dot(X[i][k]) + bu[k]))
    return sigmod(ui)


def cal_v(j, itemdim, item_metapathnum, pv, Wv, bv, Y):
    """calculate item embedding"""
    vj = np.zeros(itemdim)
    for k in range(item_metapathnum):
        vj += pv[j][k] * sigmod((Wv[k].dot(Y[j][k]) + bv[k]))
    return sigmod(vj)


def get_rating(i, j, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum, pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y):
    """calculate rating for given user and item"""
    ui = cal_u(i, userdim, user_metapathnum, pu, Wu, bu, X)
    vj = cal_v(j, itemdim, item_metapathnum, pv, Wv, bv, Y)
    return U[i, :].dot(V[j, :]) + reg_u * ui.dot(H[j, :]) + reg_v * E[i, :].dot(vj)


def maermse(T, i, j, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum, pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y):
    """calcualte error"""
    m = 0.0
    mae = 0.0
    rmse = 0.0
    n = 0
    for t in T:
        n += 1
        i = t[0]
        j = t[1]
        r = t[2]
        r_p = get_rating(i, j, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
                         pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y)

        m = abs(r_p - r)
        mae += m
        rmse += m * m
    mae = mae * 1.0 / n
    rmse = sqrt(rmse * 1.0 / n)
    return mae, rmse


def recommend(steps, R, T, V, U, H, E, X, Y, user_metapathnum, item_metapathnum, userdim, itemdim,
              pu, Wu, bu, pv, Wv, bv, reg_u, reg_v, beta_e, beta_h, beta_p, beta_w, beta_b, delta, error_level):
    """opimize the object function and calculate predictin on test data"""

    print('\nStart training')
    mae = []
    rmse = []
    starttime = datetime.now()
    perror = 99999
    cerror = 9999
    errorlevel = error_level
    n = len(R)

    for step in range(steps):
        total_error = 0.0
        for t in R:
            i = t[0]
            j = t[1]
            rij = t[2]

            rij_t = get_rating(i, j, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
                               pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y)
            eij = rij - rij_t
            total_error += eij * eij

            U_g = -eij * V[j, :] + beta_e * U[i, :]
            V_g = -eij * U[i, :] + beta_h * V[j, :]

            U[i, :] -= delta * U_g
            V[j, :] -= delta * V_g

            ui = cal_u(i, userdim, user_metapathnum, pu, Wu, bu, X)
            for k in range(user_metapathnum):
                x_t = sigmod(Wu[k].dot(X[i][k]) + bu[k])
                pu_g = reg_u * -eij * \
                    (ui * (1-ui) * H[j, :]).dot(x_t) + beta_p * pu[i][k]
                Wu_g = reg_u * -eij * pu[i][k] * np.array([ui * (1-ui) * x_t * (
                    1-x_t) * H[j, :]]).T.dot(np.array([X[i][k]])) + beta_w * Wu[k]
                bu_g = reg_u * -eij * ui * \
                    (1-ui) * pu[i][k] * H[j, :] * \
                    x_t * (1-x_t) + beta_b * bu[k]

                pu[i][k] -= 0.1 * delta * pu_g
                Wu[k] -= 0.1 * delta * Wu_g
                bu[k] -= 0.1 * delta * bu_g

            # H_g = reg_u * -eij * ui + beta_h * H[j, :]
            H_g = reg_u * -eij * ui + beta_e * H[j, :]
            H[j, :] -= delta * H_g

            vj = cal_v(j, itemdim, item_metapathnum, pv, Wv, bv, Y)
            for k in range(item_metapathnum):
                y_t = sigmod(Wv[k].dot(Y[j][k]) + bv[k])
                pv_g = reg_v * -eij * \
                    (vj * (1-vj) * E[i, :]).dot(y_t) + beta_p * pv[j][k]
                Wv_g = reg_v * -eij * pv[j][k] * np.array([vj * (1-vj) * y_t * (
                    1 - y_t) * E[i, :]]).T.dot(np.array([Y[j][k]])) + beta_w * Wv[k]
                bv_g = reg_v * -eij * vj * \
                    (1-vj) * pv[j][k] * E[i, :] * \
                    y_t * (1-y_t) + beta_b * bv[k]

                pv[j][k] -= 0.1 * delta * pv_g
                Wv[k] -= 0.1 * delta * Wv_g
                bv[k] -= 0.1 * delta * bv_g

            # E_g = reg_v * -eij * vj + 0.01 * E[i, :]
            E_g = reg_v * -eij * vj + beta_h * E[i, :]
            E[i, :] -= delta * E_g

        perror = cerror
        cerror = total_error / n

        delta = 0.93 * delta

        if(abs(perror - cerror) < errorlevel):
            break

        print('Step {} finished'.format(step))
        print('cerror: ', sqrt(cerror))

        # Calculate MAE and RSME in test data
        MAE, RMSE = maermse(T, i, j, U, V, reg_u, H, reg_v, E, userdim,
                            user_metapathnum, pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y)
        mae.append(MAE)
        rmse.append(RMSE)
        # print 'MAE, RMSE ', MAE, RMSE
        endtime = datetime.now()
        print('time: ', endtime - starttime)

    print('\nTraining finished')
    print('MAE: ', min(mae), ' RMSE: ', min(rmse))

    return U, V, pu, Wu, bu, H, pv, Wv, bv, E


def get_reverse_index(file_path):
    """reverse index of node ids"""
    num_file = pd.read_csv(file_path, header=None)
    num_file.columns = ['node_id', 'reindex']
    return num_file.set_index('reindex').to_dict()


def reverse_index(src_file, dst_file, T, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
                  pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y):
    """output result on original ids"""
    srcFile = get_reverse_index(src_file)
    dstFile = get_reverse_index(dst_file)
    # data = pd.read_csv(file_path, sep="\t", header=None)

    results = []
    for t in T:
        i = t[0]
        j = t[1]
        rij = t[2]

        rij_t = get_rating(i, j, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
                           pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y)
        i_nodeid = srcFile['node_id'][i]
        j_nodeid = dstFile['node_id'][j]
        results.append([i, j, i_nodeid, j_nodeid, rij, rij_t,
                       abs(rij-rij_t), abs((rij-rij_t)/rij)])

    return results


def get_output(src_file, dst_file, T, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
               pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y, user_metapathdims, item_metapathdims):
    # dimensions of matrix
    print('U shape: {}, V shape: {}, pu shape: {}, H shape: {}, pv shape: {}, E shape: {}'.format(U.shape, V.shape,
                                                                                                  pu.shape, H.shape, pv.shape, E.shape))

    print('Wu shape: ({}, {}, {})'.format(
        len(Wu), len(Wu[0]), len(Wu[0][0])))
    print('bu shape: ({}, {})'.format(len(bu), len(bu[0])))
    print('Wv shape: ({}, {}, {})'.format(
        len(Wv), len(Wv[0]), len(Wv[0][0])))
    print('bv shape: ({}, {})'.format(len(bv), len(bv[0])))

    print('X shape: ({}, {}, {})'.format(len(X), len(X[0]), len(X[0][0])))
    print('Y shape: ({}, {}, {})'.format(len(Y), len(Y[0]), len(Y[0][0])))

    print('User metapathnum: {}, user metapathdims: {}'.format(
        user_metapathnum, user_metapathdims))
    print('Item metapathnum: {}, item metapathdims: {}'.format(
        item_metapathnum, item_metapathdims))

    # results evaluation of test data
    results = reverse_index(src_file, dst_file, T, U, V, reg_u, H, reg_v, E, userdim, user_metapathnum,
                            pu, Wu, bu, X, itemdim, item_metapathnum, pv, Wv, bv, Y)
    results = pd.DataFrame(results)
    results.columns = ['p_id_reindex', 'b_id_reindex',
                       'p_id', 'b_id', 'rat', 'pred', 'diff', 'diff_pct']
    results.to_csv('../results/test_data_prediction.csv',
                   encoding='utf8', index=None)

    print('\nThe results are as follows:\n')
    print(results.head())
