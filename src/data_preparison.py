# encoding=utf-8

import random
import pandas as pd
from py2neo import Graph

from utils import reindex
from config import url, user_name, pass_word, db_name
from config import train_rate


##############################################################################################
# 0. Connect to MMKG
##############################################################################################

MMKG_Graph = Graph(url, auth=(user_name, pass_word), name=db_name)


##############################################################################################
# 1. Prepare relationship matrix data
##############################################################################################

# prepare Placement-Banner data
query = """
MATCH (a:Placement)-[r:Click]-(b:Banner) RETURN id(a), id(b), r.CTR, r.Clicks
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Placement-Banner relationships:", data.shape)
data.to_csv("../data/RB.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-ProductType data
query = """
MATCH (a:Banner)-[r:Has_Product_Type]-(b:ProductType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-ProductType relationships:", data.shape)
data.to_csv("../data/BPr.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-ShapeType data
query = """
MATCH (a:Banner)-[r:Has_Shape]-(b:ShapeType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-ShapeType relationships:", data.shape)
data.to_csv("../data/BSh.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-SizeType data
query = """
MATCH (a:Banner)-[r:Has_Size]-(b:SizeType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-SizeType relationships:", data.shape)
data.to_csv("../data/BSi.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-Label data
query = """
MATCH (a:Banner)-[r:Has_Label]-(b:Label) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-Label relationships:", data.shape)
data.to_csv("../data/BLa.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-Background data
query = """
MATCH (a:Banner)-[r:Has_Background]-(b:Background) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-Background relationships:", data.shape)
data.to_csv("../data/BBa.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-Product data
query = """
MATCH (a:Banner)-[r:Has_Product]-(b:Product) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-Product relationships:", data.shape)
data.to_csv("../data/BP.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Banner-Logo data
query = """
MATCH (a:Banner)-[r:Has_Logo]-(b:Logo) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Banner-Logo relationships:", data.shape)
data.to_csv("../data/BLo.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Label-TextType data
query = """
MATCH (a:Label)-[r:Has_Text_Type]-(b:TextType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Label-TextType relationships:", data.shape)
data.to_csv("../data/LaT.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Label-Location data
query = """
MATCH (a:Label)-[r:Has_Loc]-(b:LocationType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Label-LocationType relationships:", data.shape)
data.to_csv("../data/LaLc.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Logo-Location data
query = """
MATCH (a:Logo)-[r:Has_Loc]-(b:LocationType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Logo-LocationType relationships:", data.shape)
data.to_csv("../data/LoLc.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Product-Location data
query = """
MATCH (a:Product)-[r:Has_Loc]-(b:LocationType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Product-LocationType relationships:", data.shape)
data.to_csv("../data/PLc.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Product-ColorType data
query = """
MATCH (a:Product)-[r:Has_Color]-(b:ColorType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Product-ColorType relationships:", data.shape)
data.to_csv("../data/PC.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# prepare Background-ColorType data
query = """
MATCH (a:Background)-[r:Has_Color]-(b:ColorType) RETURN id(a), id(b), 1
"""
data = pd.DataFrame(MMKG_Graph.run(query))
print("Size of Background-ColorType relationships:", data.shape)
data.to_csv("../data/BaC.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

print("Preparing relationship matrix data is finished!")


##############################################################################################
# 2. Get the number of each entity in the graph
##############################################################################################

num = []

# Placement
query = """
MATCH (a:Placement) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Placement.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# Banner
query = """
MATCH (a:Banner) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Banner.csv", encoding='utf8',
            index=False, header=False)
num.append(data.shape[0])

# ProductType
query = """
MATCH (a:ProductType) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_ProductType.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# ShapeType
query = """
MATCH (a:ShapeType) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_ShapeType.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# SizeType
query = """
MATCH (a:SizeType) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_SizeType.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# Background
query = """
MATCH (a:Background) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Background.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# Product
query = """
MATCH (a:Product) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Product.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# Logo
query = """
MATCH (a:Logo) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Logo.csv", encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# Label
query = """
MATCH (a:Label) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Label.csv", encoding='utf8',
            index=False, header=False)
num.append(data.shape[0])

# Image
query = """
MATCH (a:Image) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Image.csv", encoding='utf8',
            index=False, header=False)
num.append(data.shape[0])

# ColorType
query = """
MATCH (a:ColorType) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_ColorType.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# LocationType
query = """
MATCH (a:LocationType) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_LocationType.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# TextType
query = """
MATCH (a:TextType) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_TextType.csv",
            encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# Text
query = """
MATCH (a:Text) RETURN id(a)
"""
data = pd.DataFrame(MMKG_Graph.run(query))
data['index'] = data.index.tolist()
data.to_csv("../data/num_Text.csv", encoding='utf8', index=False, header=False)
num.append(data.shape[0])

# num
print("The numbers of different entites are: ", num)
data = pd.DataFrame(num)
data.to_csv("../data/num.csv", encoding='utf8', index=False, header=False)

print("Getting the number of each entity in the graph is finished!")


##############################################################################################
# 3. Reindex relationship matrix
##############################################################################################

# reindex RB matrix
data = reindex("../data/RB.txt", "../data/num_Placement.csv",
               "../data/num_Banner.csv")
data.to_csv("../data/RB_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BPr matrix
data = reindex("../data/BPr.txt", "../data/num_Banner.csv",
               "../data/num_ProductType.csv")
data.to_csv("../data/BPr_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BSh matrix
data = reindex("../data/BSh.txt", "../data/num_Banner.csv",
               "../data/num_ShapeType.csv")
data.to_csv("../data/BSh_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BSi matrix
data = reindex("../data/BSi.txt", "../data/num_Banner.csv",
               "../data/num_SizeType.csv")
data.to_csv("../data/BSi_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BLa matrix
data = reindex("../data/BLa.txt", "../data/num_Banner.csv",
               "../data/num_Label.csv")
data.to_csv("../data/BLa_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BBa matrix
data = reindex("../data/BBa.txt", "../data/num_Banner.csv",
               "../data/num_Background.csv")
data.to_csv("../data/BBa_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BP matrix
data = reindex("../data/BP.txt", "../data/num_Banner.csv",
               "../data/num_Product.csv")
data.to_csv("../data/BP_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BLo matrix
data = reindex("../data/BLo.txt", "../data/num_Banner.csv",
               "../data/num_Logo.csv")
data.to_csv("../data/BLo_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex LaT matrix
data = reindex("../data/LaT.txt", "../data/num_Label.csv",
               "../data/num_TextType.csv")
data.to_csv("../data/LaT_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex LaLc matrix
data = reindex("../data/LaLc.txt", "../data/num_Label.csv",
               "../data/num_LocationType.csv")
data.to_csv("../data/LaLc_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex LoLc matrix
data = reindex("../data/LoLc.txt", "../data/num_Logo.csv",
               "../data/num_LocationType.csv")
data.to_csv("../data/LoLc_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex PLc matrix
data = reindex("../data/PLc.txt", "../data/num_Product.csv",
               "../data/num_LocationType.csv")
data.to_csv("../data/PLc_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex PC matrix
data = reindex("../data/PC.txt", "../data/num_Product.csv",
               "../data/num_ColorType.csv")
data.to_csv("../data/PC_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

# reindex BaC matrix
data = reindex("../data/BaC.txt", "../data/num_Background.csv",
               "../data/num_ColorType.csv")
data.to_csv("../data/BaC_reindex.txt", encoding='utf8',
            index=False, header=None, sep='\t', mode='w')

print("Reindexing relationship matrix is finished!")


##############################################################################################
# 4. cut-data into train data and test data
##############################################################################################

rel_mat = []

with open('../data/RB_reindex.txt', 'r') as infile:
    for line in infile.readlines():
        user, item, rating = line.strip().split('\t')
        rel_mat.append([user, item, rating])

random.shuffle(rel_mat)

train_num = int(len(rel_mat) * train_rate)

with open('../data/RB_reindex' + '.train', 'w') as trainfile,\
        open('../data/RB_reindex' + '.test', 'w') as testfile:
    for r in rel_mat[:train_num]:
        trainfile.write('\t'.join(r) + '\n')
    for r in rel_mat[train_num:]:
        testfile.write('\t'.join(r) + '\n')

print("Data split has been finished.")
