# MPAdBanner

Meta-path based AdBanner recommendation engine

## Install packages
* numpy
* pandas
* scipy
* py2neo
* deepwalk
    * 1) cd deepwalk
    * 2) pip install -r requirements.txt
        * wheel>=0.23.0
        * Cython>=0.20.2
        * six>=1.7.3
        * gensim=3.8.1 (this version is needed)
        * scipy>=0.15.0
        * psutil>=2.1.1
        * networkx>=2.0
    * 3) python setup.py install


## How to start

1. Run data_preparison.py (or data_preparison.ipynb) to get binary relationship data from MMKG
2. Run metapathGeneration.py to generate all meta-paths
3. Run embeddingGeneration.py to generate embeddings based on all meta-paths
4. Run embeddingFusion.py to get final embeddings and with application to recommendation. 
5. Find results of prediction under "./results/test_data_prediction.csv"

## Some details
1. The format of "./results/test_data_prediction.csv":
  The 8 columns of results are ['p_id_reindex', 'b_id_reindex', 'p_id', 'b_id', 'rat', 'pred', 'diff', 'diff_pct']

